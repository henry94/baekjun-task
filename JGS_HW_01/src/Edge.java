
public class Edge {
	private int _tailVertex;
	private int _headVertex;
	
	public Edge(int givenTailVertex, int givenHeadVertex) {
		this._tailVertex = givenTailVertex;
		this._headVertex = givenHeadVertex;
	}
	public void setTailVertex(int newTailVertex) {
		this._tailVertex = newTailVertex;
	}
	public int tailVertex() {
		return this._tailVertex;
		
	}
	public void setHeadVertex(int newHeadVertex) {
		this._headVertex = newHeadVertex;
	}
	public int headVertex() {
		return this._headVertex;
		
	}
	public int get_headVertex() {
		return _headVertex;
	}
	public void set_headVertex(int _headVertex) {
		this._headVertex = _headVertex;
	}
	public int get_tailVertex() {
		return _tailVertex;
	}
	public void set_tailVertex(int _tailVertex) {
		this._tailVertex = _tailVertex;
	}
}
